from django.db import models


class Dieta(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre')
    precio = models.DecimalField(max_digits=18, decimal_places=4, verbose_name='Precio')
    npo = models.BooleanField(default=False,  verbose_name='Dieta para pacientes NPO')
    dieta_personal = models.BooleanField(default=False, verbose_name='Dieta para personal')

    class Meta:
        app_label = 'catalogos'
        verbose_name = 'dieta'
        verbose_name_plural = 'dietas'

    def __str__(self):
        return self.nombre
