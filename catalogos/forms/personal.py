from catalogos.models import Personal
from utils.FormMixim import FieldSetModelFormMixin


class PersonalForm(FieldSetModelFormMixin):
    class Meta:
        model = Personal

        fields = [
            'id', 'nombre', 'tipo_personal'
        ]
