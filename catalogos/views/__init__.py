from .usuarios import UserCreateView, UsersListView, UsersListJson, UserUpdateView, UserDeleteJsonView
from .servicios import ServiciosListView, ServiciosListJson, ServicioCreateView, ServicioUpdateView,\
    ServicioDeleteJsonView
from .tipos_personal import TiposPersonalListView, TiposPersonalListJson, TipoPersonalCreateView,\
    TipoPersonalUpdateView, TipoPersonalDeleteJsonView
from .personal import PersonalListView, PersonalListJson, PersonalCreateView, PersonalUpdateView, PersonalDeleteJsonView
from .jornadas import JornadasListView, JornadasListJson, JornadaCreateView, JornadaUpdateView, JornadaDeleteJsonView
from .dietas import DietasListView, DietasListJson, DietaCreateView, DietaUpdateView, DietaDeleteJsonView
