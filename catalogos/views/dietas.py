from django.http import JsonResponse

# Create your views here.
from django.views.generic import TemplateView, CreateView, UpdateView
from django_datatables_view.base_datatable_view import BaseDatatableView

from catalogos.forms import DietaForm
from catalogos.models import Dieta
from utils.ResponseMixim import CreateFormResponseMixin, UpdateFormResponseMixin, JsonDeleteView
from django.contrib.humanize.templatetags.humanize import intcomma


class DietasListView(TemplateView):
    template_name = 'dietas/list.html'


class DietasListJson(BaseDatatableView):
    model = Dieta
    columns = ['id', 'nombre', 'precio', 'npo', 'dieta_personal']
    order_columns = ['id', 'nombre', 'precio', 'npo', 'dieta_personal']

    def render_column(self, row, column):
        if column == 'precio':
            precio = round(float(row.precio), 2)
            return "Q %s%s" % (intcomma(int(precio)), ("%0.2f" % precio)[-3:])
        if column == 'npo':
            if row.npo:
                return 'Si'
            else:
                return 'No'
        if column == 'dieta_personal':
            if row.dieta_personal:
                return 'Si'
            else:
                return 'No'
        else:
            return super().render_column(row, column)


class DietaCreateView(CreateView, CreateFormResponseMixin):
    template_name = 'dietas/create.html'
    model = Dieta
    form_class = DietaForm


class DietaUpdateView(UpdateView, UpdateFormResponseMixin):
    template_name = 'dietas/update.html'
    model = Dieta
    form_class = DietaForm


class DietaDeleteJsonView(JsonDeleteView):
    model = Dieta

    def post(self, request, *args, **kwargs):
        super().post(request, args, kwargs)
        return JsonResponse({'result': 'OK', 'id': kwargs.get('pk')})
