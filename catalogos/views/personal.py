from django.http import JsonResponse

# Create your views here.
from django.views.generic import TemplateView, CreateView, UpdateView
from django_datatables_view.base_datatable_view import BaseDatatableView

from catalogos.forms import PersonalForm
from catalogos.models import Personal
from utils.ResponseMixim import CreateFormResponseMixin, UpdateFormResponseMixin, JsonDeleteView


class PersonalListView(TemplateView):
    template_name = 'personal/list.html'


class PersonalListJson(BaseDatatableView):
    model = Personal
    columns = ['id', 'nombre']
    order_columns = ['id', 'nombre']


class PersonalCreateView(CreateView, CreateFormResponseMixin):
    template_name = 'personal/create.html'
    model = Personal
    form_class = PersonalForm


class PersonalUpdateView(UpdateView, UpdateFormResponseMixin):
    template_name = 'personal/update.html'
    model = Personal
    form_class = PersonalForm


class PersonalDeleteJsonView(JsonDeleteView):
    model = Personal

    def post(self, request, *args, **kwargs):
        super().post(request, args, kwargs)
        return JsonResponse({'result': 'OK', 'id': kwargs.get('pk')})
