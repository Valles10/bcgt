from django.urls import path

from . import views

app_name = 'catalogos'
urlpatterns = [
    # Urls de usuarios
    path('users/', views.UsersListView.as_view(), name='users'),
    path('users/users-json/', views.UsersListJson.as_view(), name='users-json'),
    path('users/user-create/', views.UserCreateView.as_view(), name='user-create'),
    path('users/<int:pk>/user-update/', views.UserUpdateView.as_view(), name='user-update'),
    path('users/<int:pk>/delete-update/', views.UserDeleteJsonView.as_view(), name='user-delete'),

    # Urls de servicios
    path('servicios/', views.ServiciosListView.as_view(), name='servicios'),
    path('servicios/articulos-json/', views.ServiciosListJson.as_view(), name='servicios-json'),
    path('servicios/articulo-create/', views.ServicioCreateView.as_view(), name='servicio-create'),
    path('servicios/<int:pk>/articulo-update/', views.ServicioUpdateView.as_view(), name='servicio-update'),
    path('servicios/<int:pk>/articulo-delete/', views.ServicioDeleteJsonView.as_view(), name='servicio-delete'),

    # Urls de tipos de personal
    path('tipos-personal/', views.TiposPersonalListView.as_view(), name='tipos-personal'),
    path('tipos-personal/tipos-personal-json/', views.TiposPersonalListJson.as_view(), name='tipos-personal-json'),
    path('tipos-personal/tipo-personal-create/', views.TipoPersonalCreateView.as_view(), name='tipo-personal-create'),
    path('tipos-personal/<int:pk>/tipo-personal-update/', views.TipoPersonalUpdateView.as_view(),
         name='tipo-personal-update'),
    path('tipos-personal/<int:pk>/tipo-personal-delete/', views.TipoPersonalDeleteJsonView.as_view(),
         name='tipo-personal-delete'),

    # Urls de personal
    path('personal/', views.PersonalListView.as_view(), name='personal'),
    path('personal/personal-json/', views.PersonalListJson.as_view(), name='personal-json'),
    path('personal/personal-create/', views.PersonalCreateView.as_view(), name='personal-create'),
    path('personal/<int:pk>/personal-update/', views.PersonalUpdateView.as_view(),
         name='personal-update'),
    path('personal/<int:pk>/personal-delete/', views.PersonalDeleteJsonView.as_view(),
         name='personal-delete'),

    # Urls de jornadas
    path('jornadas/', views.JornadasListView.as_view(), name='jornadas'),
    path('jornadas/jornadas-json/', views.JornadasListJson.as_view(), name='jornadas-json'),
    path('jornadas/jornada-create/', views.JornadaCreateView.as_view(), name='jornada-create'),
    path('jornadas/<int:pk>/jornada-update/', views.JornadaUpdateView.as_view(),
         name='jornada-update'),
    path('jornadas/<int:pk>/jornada-delete/', views.JornadaDeleteJsonView.as_view(),
         name='jornada-delete'),

    # Urls de dietas
    path('dietas/', views.DietasListView.as_view(), name='dietas'),
    path('dietas/dietas-json/', views.DietasListJson.as_view(), name='dietas-json'),
    path('dietas/dieta-create/', views.DietaCreateView.as_view(), name='dieta-create'),
    path('dietas/<int:pk>/dieta-update/', views.DietaUpdateView.as_view(), name='dieta-update'),
    path('dietas/<int:pk>/dieta-delete/', views.DietaDeleteJsonView.as_view(), name='dieta-delete'),
]
