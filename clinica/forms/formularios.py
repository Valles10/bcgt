from clinica.models import Paciente, Doctor, Cita, Categoria, Diagnostico, Medida, Peso, Fisioterapia, Quiropractica
from utils.FormMixim import FieldSetModelFormMixin, LargeFormMixin


class PacienteForm(FieldSetModelFormMixin):
    class Meta:
        model = Paciente
        fields = [
            'nombre', 'apellido', 'direccion', 'telefono', 'edad', 'dpi', 'genero'
        ]


class DoctorForm(FieldSetModelFormMixin):
    class Meta:
        model = Doctor
        fields = '__all__'


class CategoriaForm(FieldSetModelFormMixin):
    class Meta:
        model = Categoria
        fields = '__all__'


class CitaForm(FieldSetModelFormMixin):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['paciente'].queryset = Paciente.objects.filter(estado=1)
        self.fields['doctor'].queryset = Doctor.objects.filter(estado=1)

        self.fields['paciente'].widget.attrs['class'] = "select2"

    class Meta:
        model = Cita
        fields = '__all__'


class PesoForm(FieldSetModelFormMixin):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['imc'].widget.attrs['readonly'] = True
        self.fields['peso'].widget.attrs['step'] = "1"
        self.fields['peso'].widget.attrs['min'] = "0"
        self.fields['estatura'].widget.attrs['step'] = "1"
        self.fields['estatura'].widget.attrs['min'] = "0"

    class Meta:
        model = Peso
        fields = [
            'peso', 'estatura', 'imc', 'grasa', 'agua', 'masa_muscular', 'complexion_fisica', 'metabolismo_basal',
            'edad_metabolica', 'masa_osea'
        ]


class MedidaForm(FieldSetModelFormMixin):
    class Meta:
        model = Medida
        fields = ['abdomen_superior', 'cintura', 'abdomen_inferior', 'circunferencia_brazo',
                  'circunferencia_muslo']


class FisioterapiaForm(FieldSetModelFormMixin):
    class Meta:
        model = Fisioterapia
        fields = ['precio_f']


class QuiropracticaForm(FieldSetModelFormMixin):
    class Meta:
        model = Quiropractica
        fields = ['precio_q']


class DiagnosticoForm(LargeFormMixin):
    class Meta:
        model = Diagnostico
        fields = ['id', 'fecha', 'diagnostico', 'reseta', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado',
                  'domingo']