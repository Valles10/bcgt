from django.urls import path

from . import views

app_name = 'clinica'
urlpatterns = [
    # Urls de pacientes
    path('pacientes/', views.PacientesListView.as_view(), name='pacientes'),
    path('pacientes/pacientes-json/', views.PacientesListJson.as_view(), name='pacientes-json'),
    path('pacientes/paciente-create/', views.PacienteCreateView.as_view(), name='paciente-create'),
    path('pacientes/<int:pk>/paciente-update/', views.PacienteUpdateView.as_view(), name='paciente-update'),
    path('pacientes/<int:pk>/paciente-delete/', views.PacienteDeleteJsonView.as_view(), name='paciente-delete'),
    path('pacientes/<int:pk>/paciente-details/', views.PacienteDetallesView.as_view(), name='paciente-details'),
    path('pacientes/<int:pk>/paciente-inactivo/', views.PacienteInactivoJsonView.as_view(), name='paciente-inactivo'),

    # Urls de doctores
    path('doctores/', views.DoctoresListView.as_view(), name='doctores'),
    path('doctores/doctores-json/', views.DoctoresListJson.as_view(), name='doctores-json'),
    path('doctores/doctor-create/', views.DoctorCreateView.as_view(), name='doctor-create'),
    path('doctores/<int:pk>/menu-update/', views.DoctorUpdateView.as_view(), name='doctor-update'),
    path('doctores/<int:pk>/delete-update/', views.DoctorDeleteJsonView.as_view(), name='doctor-delete'),
    path('doctores/<int:pk>/doctore-details/', views.DoctorDetallesView.as_view(), name='doctor-details'),
    path('doctores/<int:pk>/doctore-estado/', views.DoctorEstadoJsonView.as_view(), name='doctor-estado'),
    # Urls de menus
    path('categorias/', views.CategoriasListView.as_view(), name='categorias'),
    path('categorias/categorias-json/', views.CategoriasListJson.as_view(), name='categorias-json'),
    path('categorias/categoria-create/', views.CategoriaCreateView.as_view(), name='categoria-create'),
    path('categorias/<int:pk>/categoria-update/', views.CategoriaUpdateView.as_view(), name='categoria-update'),
    path('categorias/<int:pk>/cateogria-delete/', views.CategoriaDeleteJsonView.as_view(), name='categoria-delete'),

    # Urls de citas
    path('citas/', views.CitasListView.as_view(), name='citas'),
    path('citas/citas-json/', views.CitasListJson.as_view(), name='citas-json'),
    path('citas/cita-create/', views.CitaCreateView.as_view(), name='cita-create'),
    path('citas/<int:pk>/menu-update/', views.CitaUpdateView.as_view(), name='cita-update'),
    path('citas/<int:pk>/delete-update/', views.CitaDeleteJsonView.as_view(),
         name='cita-delete'),

    # Urls de Diagnosticos
    path('diagnosticos/<int:cita_id>/diagnostico-new/', views.DiagnosticoNewView.as_view(), name='diagnostico-new'),
    path('diagnosticos/<int:pk>/diagnostico-detatils/', views.DiagnosticoDetailsView.as_view(),
         name='diagnostico-details'),

    # Urls de historial
    path('historial/', views.HistorialListView.as_view(), name='historial'),
    path('historial/historial-json/', views.HistorialListJson.as_view(), name='historial-json'),
]
