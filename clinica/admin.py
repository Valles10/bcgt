from django.contrib import admin

# Register your models here.
from clinica.models import Categoria


class CategoriasAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre')


admin.site.register(Categoria, CategoriasAdmin)
